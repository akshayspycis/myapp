import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isBlank, isNotEmpty, isNotBlank } from 'src/shared/utility';
import { UsersService } from '../service/users/users.service';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  userdetail: FormGroup;
  inputpatternCheck: any = /^(?:[A-Z\d][A-Z\d_-]{5,10}|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i;
  error: any;
  smsg: any;
  user:any;
  userdetails:any[];
  backuserdetails:any[];
  isshow:boolean=false;

  constructor(private formBuilder: FormBuilder, private usersvr: UsersService, public toastController: ToastController) {
    this.userdetail = this.formBuilder.group({
      username: '',
      password: '',
    });
    this.usersvr.getUserObservable().subscribe(user=>{
      if(user){
        this.user=user;
        this.isshow=true;
        this.loadUserAll();
      }else{
        this.user=null;
        this.isshow=false;
        this.userdetails=[];
        this.backuserdetails=[];
      }
    })
  }

  logout(){
    this.usersvr.logout();
  }

  loadUserAll(){
    this.usersvr.loadUserAll().then(users=>{
      this.userdetails=users;
      this.backuserdetails=users;
    })
  }
  ionSearchAction(event){
    this.userdetails=this.backuserdetails;
    if(event && event.detail && isNotBlank(event.detail.value)){
      this.userdetails=this.backuserdetails.filter(o=>o.username.startsWith(event.detail.value));
    }
  }

  onSubmit(value) {
    if (this.validationForm(this.userdetail)) {
      this.loginUser();
    }
  }

  createUser() {
    if (this.validationForm(this.userdetail)) {
      this._createUser();
    }
  }
  _createUser() {
    let user = {
      username: this.userdetail.controls.username.value,
      password: this.userdetail.controls.password.value,
      email: this.userdetail.controls.username.value + "@gmail.com",
    }
    this.usersvr.insertUser(user).then(res => {
      console.log(res)
      this.userdetail.reset();
      this.presentToast("User created successfully");
      if(isNotEmpty(this.userdetails) ){
        this.userdetails.push(res);
      }
    }).catch(err => {
      if (err && err.error && err.error.error && err.error.error.message) {
        if (err.error.error.message.includes("Email already exists"))
          this.error = "User already exists";
        else this.error = "Internal Server Error";
      } else {
        this.error = "Internal Server Error";
      }
    })

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'middle',
      duration: 2000
    });
    toast.present();
  }


  loginUser() {
    let user = {
      username: this.userdetail.controls.username.value,
      password: this.userdetail.controls.password.value,
      email: this.userdetail.controls.username.value + "@gmail.com",
    }
    this.usersvr.loginUser(user).then(res => {
      this.userdetail.reset();
      this.presentToast("Login successfully");
    }).catch(err => {
      this.error = "Unauthorized User";
    })
  }

  onTextChange() {
    this.error = null;
    this.smsg = null;
  }

  validationForm(userdetail) {
    this.error = null;
    if (isBlank(userdetail.controls.username.value)) {
      this.error = "Please provide the valid username";
      return false;
    } else if (isBlank(userdetail.controls.password.value)) {
      this.error = "Please provide the valid password";
      return false;
    } else {
      return true;
    }
  }
}
