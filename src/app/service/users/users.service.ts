import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {PLATFORM_API_HOST,URL_INSERT_USER,URL_USER_LOGIN, URL_USER_ID,URL_USER_All} from '../../../shared/constant';
import { catchError, timeout, map } from 'rxjs/operators';
import { TokenService } from '../token/token.service';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  userdetails:any[];
  userdetail:any;
  userdetailsub:Subject<any>;
  constructor(public http: HttpClient,public token:TokenService) {
    this.userdetailsub=new BehaviorSubject(null);
  }

  loadUser(id){
    let url = PLATFORM_API_HOST + URL_USER_ID+id;
    console.log(url);
    return this.http.get<any>(url)
    .pipe(
      map(r => { 
        return r;
      })
    ).toPromise();
  }

  loadUserAll() {
    let url = PLATFORM_API_HOST + URL_USER_All;
    return this.http.get<any>(url)
    .pipe(
      map(r => { 
        this.userdetail=r;
        return r;
      })
    ).toPromise();
  }

  loginUser(body){
    let url = PLATFORM_API_HOST + URL_USER_LOGIN;
    return this.http.post<any>(url, body)
    .pipe(
      map(r => { 
        this.setUser(r);
        return r;
      })
    ).toPromise();
  }

  setUser(r){
    this.token.setAccessToken(r.id);
    this.loadUser(r.userId).then(user=>{
      this.userdetail=user;
      this.userdetailsub.next(user);
    })
  }
  removeUser(){
    this.userdetail=null;
    this.token.setAccessToken(null);
    this.userdetailsub.next(null);
  }
  
  public getUserObservable(): Subject<any> {
    return this.userdetailsub;
  }

  insertUser(body){
    let url = PLATFORM_API_HOST + URL_INSERT_USER;
    return this.http.post<any>(url, body)
    .toPromise();
  }
  
  logout(){
    this.token.setAccessToken(null);
      this.userdetail=null;
      this.userdetailsub.next(null);
    }
}
