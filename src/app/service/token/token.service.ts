import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  token:any;
  constructor() { }
  setAccessToken(token){
    this.token=token;
  }
  getToken(){
    return this.token;
  }
}
