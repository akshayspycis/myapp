import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from "rxjs";
import { TokenService } from './token/token.service';
import { PLATFORM_API_HOST } from 'src/shared/constant';
import { isNotBlank } from 'src/shared/utility';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public token: TokenService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.startsWith(PLATFORM_API_HOST) && isNotBlank(this.token.getToken())) {
            req = req.clone({
                setHeaders: {
                    Authorization: `${this.token.getToken()}`
                },
                setParams: {
                    clientapp: 'myApp',
                    access_token: `${this.token.getToken()}`,
                }
            });
        }
        return next.handle(req);
    }
}