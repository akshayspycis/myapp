
export function trim(value: string) {
    if (value && typeof value == 'string') {
        return value.trim();
    }
    return value;
}

export function isNotEmpty(array) {
    if (array && Array.isArray(array) && array.length > 0) {
        return true;
    }
    return false;
}

export function isEmpty(array) {
    if (!array || (Array.isArray(array) && array.length == 0)) {
        return true;
    }
    return false;
}

export function isNotBlank(value: string): boolean {
    return (value && trim(value) != '');
}

export function isBlank(value: string): boolean {
    return (!value || trim(value) == '');
}
