export const PLATFORM_API_HOST: string = "https://api-server-test-1.herokuapp.com";
// export const PLATFORM_API_HOST: string = "http://localhost:3000"; 
export const URL_INSERT_USER ="/api/Users";
export const URL_USER_LOGIN ="/api/Users/login";
export const URL_USER_ID ="/api/Users/";
export const URL_USER_All ="/api/Users?filter=%7B%7D";